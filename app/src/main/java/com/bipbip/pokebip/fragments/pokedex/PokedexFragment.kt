package com.bipbip.pokebip.fragments.pokedex

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bipbip.pokebip.R
import com.bipbip.pokebip.databinding.FragmentLoginBinding
import com.bipbip.pokebip.databinding.FragmentPokedexBinding
import com.bipbip.pokebip.fragments.login.LoginViewModel
import com.bipbip.pokebip.fragments.login.LoginViewModelState
import com.bipbip.pokebip.utils.getBearerToken

private const val TAG = "PokedexFragment"

class PokedexFragment : Fragment() {

    private val model: PokedexViewModel by viewModels()
    private lateinit var adapter: PokedexAdapter
    private lateinit var binding: FragmentPokedexBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentPokedexBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = PokedexAdapter(pokedexList=  listOf(), clickListener =  { view ->
            // TODO implémenter la redirection
        }, context = requireContext())

        binding.pokedexListView.adapter = adapter
        binding.pokedexListView.layoutManager = LinearLayoutManager(context)

        model.getState().observe(viewLifecycleOwner, Observer { updateUi(it!!) })

        model.loadPokedex(getBearerToken(requireContext())!!)
    }

    private fun updateUi(state: PokedexViewModelState) {
        when (state) {
            is PokedexViewModelState.Success -> {
                adapter.updateDataSet(state.pokedex)
            }
            is PokedexViewModelState.Failure -> {
                //TODO implement ui for it
            }
            is PokedexViewModelState.Loading -> {
                //TODO implement ui for it
            }
        }
    }
}