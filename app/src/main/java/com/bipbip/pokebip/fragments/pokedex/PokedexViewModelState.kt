package com.bipbip.pokebip.fragments.pokedex

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bipbip.pokebip.Pokemon
import com.bipbip.pokebip.api.authapi.AuthApi
import com.bipbip.pokebip.api.pokedexapi.ApiPokemon
import com.bipbip.pokebip.api.pokedexapi.PokemonApi
import com.bipbip.pokebip.api.pokedexapi.mapApiPokemonListWrapperToPokemonList
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


private const val TAG = "PokedexViewModel"

sealed class PokedexViewModelState(
    open val errorMessage: String = "",
    open val pokedex: List<Pokemon> = listOf()
) {
    object Loading : PokedexViewModelState()
    data class Success(override val pokedex: List<Pokemon>) :
        PokedexViewModelState(pokedex = pokedex)

    data class Failure(override val errorMessage: String) :
        PokedexViewModelState(errorMessage = errorMessage)
}

class PokedexViewModel : ViewModel() {

    private val api: PokemonApi

    init {
        // WARNING
        // This init should be done ONCE in the app
        // Application class is a good place
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl("http://api.pokedex.com/")
            .client(client)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()

        api = retrofit.create(PokemonApi::class.java)
    }

    private val state = MutableLiveData<PokedexViewModelState>()
    fun getState(): LiveData<PokedexViewModelState> = state

    fun loadPokedex(bearerToken: String) {
        val call = api.getPokemonList(token = bearerToken)

        call.enqueue(object : Callback<List<ApiPokemon>> {
            override fun onResponse(
                call: Call<List<ApiPokemon>>,
                response: Response<List<ApiPokemon>>
            ) {
                val res = response.body()
                if (res != null) {
                    Log.d(TAG, "onResponse: $res")
                    val pokemonList = mapApiPokemonListWrapperToPokemonList(res)
                    state.value = PokedexViewModelState.Success(pokemonList)
                }
            }

            override fun onFailure(call: Call<List<ApiPokemon>>, t: Throwable) {
                if (t.message != null) {
                    state.value = PokedexViewModelState.Failure(t.message!!)
                } else {
                    state.value = PokedexViewModelState.Failure("Error api request")
                }
            }
        })
    }
}