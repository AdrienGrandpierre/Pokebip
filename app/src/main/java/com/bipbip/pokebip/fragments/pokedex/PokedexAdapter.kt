package com.bipbip.pokebip.fragments.pokedex

import android.content.Context
import android.graphics.BitmapFactory
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bipbip.pokebip.Pokemon
import com.bipbip.pokebip.databinding.RowPokedexBinding
import java.io.File


class PokedexAdapter(
    private var pokedexList: List<Pokemon>,
    private val clickListener: View.OnClickListener,
    private val context: Context
) :
    RecyclerView.Adapter<PokedexAdapter.ViewHolder>() {
    class ViewHolder(val binding: RowPokedexBinding) : RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = RowPokedexBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pokemon = pokedexList[position]
        with(holder) {
            binding.root.tag = pokemon.name
            binding.root.setOnClickListener(clickListener)

            //FIXME ne plus avoir les images dans l'app mais sur un serveur de fichier
            val otherPath: Uri = Uri.parse("android.resource://com.bipbip.pokebip/drawable/pokemon_${pokemon.natId}")
            if (otherPath.path != null) {
                val imgFile = File(otherPath.path!!)
                if (imgFile.exists()) {
                    val myBitmap = BitmapFactory.decodeFile(imgFile.absolutePath)
                    binding.pokemonImage.setImageBitmap(myBitmap)
                }
            }

            binding.pokemonID.text = pokemon.natId.toString()
            binding.pokemonName.text = pokemon.name
            binding.pokemonCaught.isEnabled = pokemon.caught

        }
    }

    override fun getItemCount(): Int = pokedexList.size

    fun updateDataSet(list: List<Pokemon>) {
        this.pokedexList = list
        notifyDataSetChanged()
    }
}