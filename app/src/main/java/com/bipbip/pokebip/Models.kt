package com.bipbip.pokebip

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Pokemon(
    val id: Int,
    val natId: Int,
    val name: String,
    val hp :String,
    val atk: String,
    val def: String,
    val spa: String,
    val spd: String,
    val spe: String,
    val type1: String,
    val type2: String?,
    val masse: String,
    val desc: String,
    val caught : Boolean
): Parcelable