package com.bipbip.pokebip.api.pokedexapi

import com.bipbip.pokebip.Pokemon

fun mapApiPokemonListWrapperToPokemonList(pokemonApiWrapper: List<ApiPokemon>): List<Pokemon> {
    val list = mutableListOf<Pokemon>()
    for (pokemonApi in pokemonApiWrapper) {
        list.add(mapPokemonApi(pokemonApi))
    }
    return list
}

fun mapPokemonApi(apiPokemon: ApiPokemon): Pokemon {
    return Pokemon(
        id = apiPokemon.id ,
        name =apiPokemon.name ,
        natId=apiPokemon.natId ,
        hp =apiPokemon.hp,
        atk=apiPokemon.atk ,
        def=apiPokemon.def ,
        spa=apiPokemon.spa ,
        spd=apiPokemon.spd ,
        spe=apiPokemon.spe ,
        type1=apiPokemon.type1 ,
        type2=apiPokemon.type2 ,
        masse=apiPokemon.masse ,
        desc=apiPokemon.desc ,
        caught = apiPokemon.caught
    )
}