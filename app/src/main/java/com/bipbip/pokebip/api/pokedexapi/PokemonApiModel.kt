package com.bipbip.pokebip.api.pokedexapi

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiPokemon(
    val id: Int,
    val natId: Int,
    val name: String,
    val hp :String,
    val atk: String,
    val def: String,
    val spa: String,
    val spd: String,
    val spe: String,
    val type1: String,
    val type2: String?,
    val masse: String,
    val desc: String,
    val caught : Boolean = false
)
