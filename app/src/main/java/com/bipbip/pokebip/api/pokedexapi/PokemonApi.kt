package com.bipbip.pokebip.api.pokedexapi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface PokemonApi {

    @GET("pokemons/")
    fun getPokemonList(
        @Header("Authorization") token : String
    ): Call<List<ApiPokemon>>

    @GET("pokemon/{pokemonID}")
    fun getPokemon(
        @Header("Authorization") token : String,
        @Path(value = "pokemonID") pokemonID: Long
    ): Call<ApiPokemon>
}